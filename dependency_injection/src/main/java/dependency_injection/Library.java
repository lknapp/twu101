package dependency_injection;
import java.io.PrintStream;


public class Library {
  private PrintStream ps;
  private Book book;

  public Library(PrintStream ps, Book book) {
    this.ps = ps;
    this.book = book;
  }

  public void printBookTitle(){

    ps.println(book.getTitle());
  }
}
